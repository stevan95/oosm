# General info

This website is powered by [Jekyll](https://jekyllrb.com/). \
For setting up Jekyll development envirionment check out these [guides](https://jekyllrb.com/docs/installation/).

We use `jekyll-minifier` plugin for code minification, read instructions [here](https://github.com/digitalsparky/jekyll-minifier)

Check out current issues [here](https://gitlab.com/stevan95/oosm/-/boards).

# Development style

## Indentation
Use **4 spaces** for **1 level** of indentation.

## Naming conventions
All development is done in English, so please refrain from using any other language in writing:
- Commit messages
- Identifiers *(more on these later)*
- Code comments

### Identifiers
Use [kebab-case](https://stackoverflow.com/questions/11273282/whats-the-name-for-hyphen-separated-case) for writing id and class names in HTML and CSS:
```html
<img src="assets/images/hklbgd.png" alt="Haklab logo" class="img-fluid" />
```
```css
.img-fluid {
    width: 100%;
    margin: auto;
}
```

For everything not mentioned in this file, try to mimic already existing code.

### File names
Use [kebab-case](https://stackoverflow.com/questions/11273282/whats-the-name-for-hyphen-separated-case) for writing file names.

## Trailing whitespaces
Do not leave trailing whitespace at the ends of lines. Some editors with smart indentation will insert whitespace at the beginning of new lines as appropriate, so you can start typing the next line of code right away. However, some such editors do not remove the whitespace if you end up not putting a line of code there, such as if you leave a blank line. As a result, you end up with lines containing trailing whitespace.


## Commit messages
Write **meaningful** and **concise** commit messages. This is important so we can quickly find out where were troublesome changes introduced instead of sifting through commit history, trying to find the line that screwed everything up.

```bash
# TL; DR

# Wrong way
git commit -m "small fix"
git commit -m "blah"
git commit -m "asdsadfaf"

# Correct way
git commit -m "Made navbar sticky. Implemented pagination on posts page."
```

**Note:** If you think you should further explain what your commit does, you can comment important lines in the commit on GitLab.
