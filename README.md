[![pipeline status](https://gitlab.com/stevan95/oosm/badges/master/pipeline.svg)](https://gitlab.com/stevan95/oosm/)

Website for a  [hackerspace](https://en.wikipedia.org/wiki/Hackerspace) in [Belgrade](https://en.wikipedia.org/wiki/Belgrade).
Live at [hklbgd.org](https://hklbgd.org/).

For contributing read [HACKING.md](/HACKING.md)
